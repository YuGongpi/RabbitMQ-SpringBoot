# RabbitMQ-SpringBoot

#### 介绍
{**以下是码云平台说明，您可以替换此简介**
码云是 OSCHINA 推出的基于 Git 的代码托管平台（同时支持 SVN）。专为开发者提供稳定、高效、安全的云端软件开发协作平台
无论是个人、团队、或是企业，都能够用码云实现代码托管、项目管理、协作开发。企业项目请看 [https://gitee.com/enterprises](https://gitee.com/enterprises)}

#### 软件架构
rabbitmq springboot


#### 环境说明

1.  多租户：appId+tenantId
2.  队列：每个租户的每个业务分一个队列
3.  动态创建队列：在发布时根据appId+tenantId，创建队列。
4.  动态消费，动态发布，声明队列，添加监听的队列
5.  不同的业务对应不同的发布者和消费者

#### 使用说明
1.  回调逻辑执行是子线程，主线程想获取执行结果，本例使用的CountDownLaunch
2.  对于每次请求多个线程访问一个变量，隔离请求可以使用Map,当让也可以用数据库。
3.  对于重发次数的计数变量采用的ThreadLocal，注意用完红后需要重置或者直接remove

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request

#### 流程图

![输入图片说明](https://images.gitee.com/uploads/images/2020/0623/220827_ad5ed484_1154738.png "屏幕截图.png")


#### 码云特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5.  码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
