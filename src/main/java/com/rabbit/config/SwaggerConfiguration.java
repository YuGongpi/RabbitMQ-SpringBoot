package com.rabbit.config;

import org.springframework.context.annotation.Configuration;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
public class SwaggerConfiguration {

   /* *//**
     * swagger2的配置文件，这里可以配置swagger2的一些基本的内容，比如扫描的包等等
     * @return
     *//*
    @Bean
    public Docket createRestfulApi(){
    	 List<Parameter> pars = new ArrayList<Parameter>();
    	 ParameterBuilder tokenPar = new ParameterBuilder();
         tokenPar.name("Authorization").description("token").modelRef(new ModelRef("string")).parameterType("header").required(true).build();
         pars.add(tokenPar.build());
         
        return new Docket(DocumentationType.SWAGGER_2)
                .directModelSubstitute(LocalDateTime.class, Date.class)
                .directModelSubstitute(LocalDate.class, String.class)
                .directModelSubstitute(LocalTime.class, String.class)
                .directModelSubstitute(ZonedDateTime.class, String.class)
        		.apiInfo(apiInfo())
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.prefboss"))  //暴露接口地址的包路径
                .build()
                .globalOperationParameters(pars);
    }

    *//**
     * 构建 api文档的详细信息函数,注意这里的注解引用的是哪个
     * @return
     *//*
    private ApiInfo apiInfo(){
        return new ApiInfoBuilder()
                //页面标题
                .title("会员积分系统")
                //版本号
                .version("2.0")
                //描述
                .description("API 描述")
                .build();
    }*/
}
