package com.rabbit.producer;

import com.rabbit.icore.*;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.amqp.rabbit.listener.SimpleMessageListenerContainer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import java.util.Map;

@RestController
@Api(tags = {"消息队列提供者"})
@RequestMapping("/producer")
public class ProducerController {

    @Autowired
    private RabbitTemplatePlus rabbitTemplatePlus;

    @Autowired
    private Exchanges exchanges;

    @Autowired
    private ConfirmCallbackImpl confirmCallback;

    @Autowired
    private ReturnCallbackImpl returnCallback;

    @Autowired @Qualifier("Reduce_SimpleMessageListenerContainer")
    private SimpleMessageListenerContainer reduce_simpleMessageListenerContainer;

    @Autowired @Qualifier("Add_SimpleMessageListenerContainer")
    private SimpleMessageListenerContainer add_simpleMessageListenerContainer;


    @ApiOperation(value = "保存删减的业务消息至队列")
    @PostMapping(value = "/reduce")
    public Result produceReduceMessage(@RequestBody Map map) {//网关检查一下格式的正确性
        //1.动态创建队列、交换机、绑定
        String queueName = "reduce_" + map.get("appId");
        exchanges.createExchangeQueueAndBinding(queueName, queueName);
        reduce_simpleMessageListenerContainer.addQueueNames(queueName);
//        AppContext.updateQueuesName();
        //2.设置回调逻辑
        rabbitTemplatePlus.setConfirmCallback(confirmCallback);//设置Confirm回调逻辑
        rabbitTemplatePlus.setReturnCallback(returnCallback);//设置Return回调逻辑

        //3.发送、等待、返回结果
        rabbitTemplatePlus.sendPlus(map,queueName);//发送
        AppContext.await(AppContext.confirmCall + queueName);//等待confirm子线程地处理结果
//        AppContext.await(AppContext.returnCall + appId);//等待return子线程地处理结果
        //你也可以自己pok一个线程，定义一个定期删除机制
        AppContext.deleteCountDownLatch(AppContext.confirmCall + queueName);
//        AppContext.deleteCountDownLatch(AppContext.returnCall + appId);
        return AppContext.getResult(queueName);//获取子线程地处理结果，并删掉
    }

    @ApiOperation(value = "保存添加业务的消息至队列")
    @PostMapping(value = "/add")
    public Result produceAddMessage(@RequestBody Map map) {//网关检查一下格式的正确性
        //1.创建队列、交换机、绑定
        String queueName = "add_" + map.get("appId");
        exchanges.createExchangeQueueAndBinding(queueName, queueName);
        add_simpleMessageListenerContainer.addQueueNames(queueName);
//        AppContext.updateQueuesName();
        //2.设置回调逻辑
        rabbitTemplatePlus.setConfirmCallback(confirmCallback);//设置Confirm回调逻辑
        rabbitTemplatePlus.setReturnCallback(returnCallback);//设置Return回调逻辑

        //3.发送、等待、返回结果
        rabbitTemplatePlus.sendPlus(map,queueName);//发送
        AppContext.await(AppContext.confirmCall + queueName);//等待confirm子线程地处理结果
//        AppContext.await(AppContext.returnCall + appId);//等待return子线程地处理结果
        //你也可以自己pok一个线程，定义一个定期删除机制
        AppContext.deleteCountDownLatch(AppContext.confirmCall + queueName);
//        AppContext.deleteCountDownLatch(AppContext.returnCall + appId);
        return AppContext.getResult(queueName);//获取子线程地处理结果，并删掉
    }
}
