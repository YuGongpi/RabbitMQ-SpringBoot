package com.rabbit;

import org.springframework.amqp.rabbit.annotation.EnableRabbit;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @EnableFeignClients 开启Feign功能
 * @author
 */
@SpringBootApplication(scanBasePackages = {"com.rabbit"})
@EnableRabbit
public class RabbitApplication {

    public static void main(String[] args) {
        SpringApplication.run(RabbitApplication.class, args);
    }

}
