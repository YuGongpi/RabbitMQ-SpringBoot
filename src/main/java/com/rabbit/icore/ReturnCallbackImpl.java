package com.rabbit.icore;

import lombok.Data;
import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.rabbit.core.RabbitAdmin;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Data
@Component
public class ReturnCallbackImpl implements RabbitTemplate.ReturnCallback {
    @Autowired
    private RabbitTemplatePlus rabbitTemplatePlus;//手动输入
    @Autowired
    private RabbitAdmin rabbitAdmin;

    /**
     * @param message :消息对象
     * @param i       ：错误码
     * @param s       ：错误信息
     * @param s1      ：交换机
     * @param s2      ：路由键
     */
    @Override
    public void returnedMessage(Message message, int i, String s, String s1, String s2) {
        try {
            if (AppContext.getCount() < 1) {
                //根据不同的异常来处理不同的逻辑，主要是没有队列，没有绑定
                rabbitAdmin.declareQueue(new Queue(s2));
                rabbitAdmin.declareBinding(new Binding(s2, Binding.DestinationType.QUEUE, s1, s2, null));//建立连接
                rabbitTemplatePlus.convertAndSend(s1, s2, message);//重发，基本上重发一次就可以了
            } else {
                //重发过又失败了，直接记录在数据库和日志
                AppContext.setResult(s2, "code", "4201");
                System.out.println("returnCallback重发失败，记录在数据库和日志。。。" + s);
                AppContext.setCount(0);//不知道有没有必要，threadLocal对应的每个线程的值在什么时候消亡，如果只是放回线程池会不会消亡？？？？
//                AppContext.countDown(AppContebxt.confirmCall + s2);//回调逻辑已经执行完成 result 已经修改，减到0，让主线程继续执行返回result，由于成功不会执行，所以这里失败直接记录数据库和日志
            }
        } catch (Exception e) {
            AppContext.setResult(s2, "code", "4202");
            e.printStackTrace();
            System.out.println("returnCallback重发时出现异常，记录在数据库和日志。。。");
            AppContext.setCount(0);//或者直接remove
//            AppContext.countDown(AppContext.confirmCall + s2);//回调逻辑已经执行完成，result 已经修改，减到0，让主线程继续执行返回result
        }
    }

}
