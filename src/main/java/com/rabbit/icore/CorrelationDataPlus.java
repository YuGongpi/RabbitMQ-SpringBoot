package com.rabbit.icore;

import lombok.Data;
import org.springframework.amqp.rabbit.connection.CorrelationData;

import java.util.List;
import java.util.Map;

@Data
//对CorrelationData的封装，用于confirm时获取相应的信息
public class CorrelationDataPlus extends CorrelationData {
    private Map body;//消息体
    private List<String> exchanges;//交换机
    private String routingKey;//路由键
    private int retryCount = 0;//重发的次数
}
