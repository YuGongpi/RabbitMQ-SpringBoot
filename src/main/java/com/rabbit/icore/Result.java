package com.rabbit.icore;

import lombok.Data;

import java.util.HashMap;

/**
 * 通用自定义返回结果
 */

@Data
public class Result extends HashMap<String, Object> {
    public static Result ok(String key, String value) {
        Result r = new Result();
        r.put(key, value);
        return r;
    }

    public static Result error(String errCode, String errMessage) {
        Result r = new Result();
        r.put("errCode", errCode);
        r.put("errMessage", errMessage);
        return r;
    }
}
